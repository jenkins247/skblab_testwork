from pages.base_page import BasePage
from selenium.webdriver.common.keys import Keys

class ContactListPage(BasePage):

    FIRST_NAME_FIELD = "//td[contains(text(),'First Name:')]/following-sibling::td/input"
    LAST_NAME_FIELD = "//td[contains(text(),'Last Name:')]/following-sibling::td/input"
    CATEGORY_DROPLIST = "//td[contains(text(),'Category:')]/following-sibling::td/select"
    BIRTHDAY_FIELD = "//td[contains(text(),'Birthday:')]/following-sibling::td/input"
    CALENDAR = "//div[@class='dateBoxPopup']"
    ADDRESS_FIELD = "//td[contains(text(),'Address:')]/following-sibling::td/textarea"
    TITLE = "//td[contains(text(), 'Contact Info')]"
    CREATE_CONTACT_BTN = "//button[contains(text(),'Create Contact')]"
    UPDATE_CONTACT_BTN = "//button[contains(text(),'Update Contact')]"
    GENERATE_BTN = "//button[contains(text(),'Generate 50 Contacts')]"

    def type_firstname(self, first_name):
        firstname_field = self.driver.find_element_by_xpath(self.FIRST_NAME_FIELD)
        firstname_field.send_keys(first_name)

    def type_lastname(self, last_name):
        lastname_field = self.driver.find_element_by_xpath(self.LAST_NAME_FIELD)
        lastname_field.send_keys(last_name)

    def select_dob_from_calendar(self):
        self.driver.find_element_by_xpath(self.BIRTHDAY_FIELD).click()
        all_dates = self.driver.find_element_by_xpath(self.CALENDAR)
        for element in all_dates:
            date = element.text
            if date == 28:
                element.click()
            break

    def type_dob(self, dob):
        birthday_field = self.driver.find_element_by_xpath(self.BIRTHDAY_FIELD)
        birthday_field.send_keys(dob)
        birthday_field.send_keys(Keys.TAB)

    def type_address(self, address):
        address_field = self.driver.find_element_by_xpath(self.ADDRESS_FIELD)
        address_field.send_keys(address)

    def click_create_button(self):
        create_button = self.driver.find_element_by_xpath(self.CREATE_CONTACT_BTN)
        create_button.click()

    def click_update_button(self):
        update_button = self.driver.find_element_by_xpath(self.UPDATE_CONTACT_BTN)
        update_button.click()