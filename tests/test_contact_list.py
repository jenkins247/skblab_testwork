from tests.base_test import BaseTest
from pages.contact_list_page import ContactListPage

class TestContactList(BaseTest):

    def setUp(self):
        BaseTest.setUp(self)
        self.contact_list = ContactListPage(self.driver)

    def test_open_contact_list(self):
        pass

    def test_add_new_contact(self):
        self.contact_list.type_firstname("John")
        self.contact_list.type_lastname("Doe")
        self.contact_list.type_dob("July 23, 1997")
        self.contact_list.type_address("99 Street St")
        self.contact_list.click_create_button()

    def test_update_existing_contact(self):
        pass

    def test_generate_contacts(self):
        pass


